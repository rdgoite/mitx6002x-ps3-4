#!/usr/bin/python

import copy
import numpy
import random

from matplotlib import pylab

#from ps3b_precompiled_27 import *
from ps3b import *

class SimulationEnvironment(object):

    def __init__(self, virusCount=0, maxPopulation=0, maxBirthProbability=0,
                 clearanceProbability=0, resistances=0, mutationProbability=0,
                 trials=0):
        self.virusCount = virusCount
        self.maxPopulation = maxPopulation
        self.maxBirthProbability = maxBirthProbability
        self.clearanceProbability = clearanceProbability
        self.resistances = resistances
        self.mutationProbability = mutationProbability
        self.trials = trials

    def createViruses(self):
        return [ResistantVirus(self.maxBirthProbability, self.clearanceProbability,
                               self.resistances, self.mutationProbability) for count in range(self.virusCount)]

    def createPatient(self):
        return TreatedPatient(self.createViruses(), self.maxPopulation)


class Simulator(object):

    def cocktailRun(self, patient, stepsBeforeGrimpex=150):
        stepsBeforeGuttagonol = 150
        for count in range(stepsBeforeGuttagonol): patient.update()
        patient.addPrescription('guttagonol')
        for count in range(stepsBeforeGrimpex): patient.update()
        patient.addPrescription('grimpex')
        finalSteps = 150
        for count in range(finalSteps - 1): patient.update()
        return patient.update()

    def cocktailSimulation(self, environment, stepsBeforeGrimpex=150):
        trials = environment.trials
        data = [None for count in range(trials)]
        for count in range(trials):
            data[count] = self.cocktailRun(environment.createPatient(), stepsBeforeGrimpex)
        return data
        
    def simulateCocktail(self, environment):
        delays = [0, 75, 150, 300]
        figure, plots = pylab.subplots(len(delays), sharex=True, sharey=True)
        for index in range(len(delays)):
            data = self.cocktailSimulation(environment, delays[index])
            plots[index].hist(data, bins=25, histtype='stepfilled')
            plots[index].set_title('delay: {}'.format(delays[index]))
        pylab.show()
            
    def simulateCocktailWithVariation(self, environment):
        variables = [0.005, 0.01, 0.015, 0.02]
        figure, plots = pylab.subplots(len(variables), sharex=True, sharey=True)
        for index in range(len(variables)):
            setup = copy.copy(environment)
            setup.mutationProbability = variables[index]
            data = self.cocktailSimulation(setup)
            plots[index].hist(data, bins=25, histtype='stepfilled')
            plots[index].set_title('value: {}'.format(variables[index]))
        pylab.show()
            
        
def simulationWithDrug(virusCount, maxPopulation, maxBirthProbability,
                       clearanceProbability, resistances, mutationProbability,
                       trials, medicationDelay=150, medicationSteps=150):
    totalTimeSteps = medicationDelay + medicationSteps

    populationAtTime = [0 for count in range(totalTimeSteps)]
    resistantPopulationAtTime = [0 for count in range(totalTimeSteps)]

    drug = 'guttagonol'

    for trial in range(trials):
        viruses = [ResistantVirus(maxBirthProbability, clearanceProbability, resistances, mutationProbability) for count in range(virusCount)]
        patient = TreatedPatient(viruses, maxPopulation)
        for timeStep in range(0, medicationDelay):
            populationAtTime[timeStep] += patient.update()
            resistantPopulationAtTime[timeStep] += patient.getResistPop([drug])
        patient.addPrescription(drug)
        for timeStep in range(medicationDelay, totalTimeSteps):
            populationAtTime[timeStep] += patient.update()
            resistantPopulationAtTime[timeStep] += patient.getResistPop([drug])

    averagePopulation = [float(population) / float(trials) for population in populationAtTime]
    averageResistantPopulation = [float(population) / float(trials) for population in resistantPopulationAtTime]

    timeAxis = range(totalTimeSteps)

    pylab.plot(timeAxis, averagePopulation)
    pylab.plot(timeAxis, averageResistantPopulation)
    pylab.legend(['All', 'Resistant'], loc='lower right')

    pylab.title('Popluation of Viruses Over {} Time Steps'.format(totalTimeSteps))
    pylab.xlabel('Time')
    pylab.ylabel('Population')
    pylab.show()

def defaultSimulationWithDrug(virusCount, maxPopulation, maxBirthProbability,
                       clearanceProbability, resistances, mutationProbability,
                       trials):
    """
    Runs simulations and plots graphs for problem 5.

    For each of numTrials trials, instantiates a patient, runs a simulation for
    150 timesteps, adds guttagonol, and runs the simulation for an additional
    150 timesteps.  At the end plots the average virus population size
    (for both the total virus population and the guttagonol-resistant virus
    population) as a function of time.

    numViruses: number of ResistantVirus to create for patient (an integer)
    maxPop: maximum virus population for patient (an integer)
    maxBirthProb: Maximum reproduction probability (a float between 0-1)        
    clearProb: maximum clearance probability (a float between 0-1)
    resistances: a dictionary of drugs that each ResistantVirus is resistant to
                 (e.g., {'guttagonol': False})
    mutProb: mutation probability for each ResistantVirus particle
             (a float between 0-1). 
    numTrials: number of simulation runs to execute (an integer)
    
    """
    simulationWithDrug(virusCount, maxPopulation, maxBirthProbability,
                       clearanceProbability, resistances, mutationProbability,
                       trials)

def perTrialSimulationWithDrug(virusCount, maxPopulation, maxBirthProbability,
                       clearanceProbability, resistances, mutationProbability,
                       trials, medicationDelay=150, medicationSteps=150):

    totalTimeSteps = medicationDelay + medicationSteps
    drug = 'guttagonol'

    populationAtTrial = [0 for count in range(trials)]
    
    for trial in range(trials):
        viruses = [ResistantVirus(maxBirthProbability, clearanceProbability, resistances, mutationProbability) for count in range(virusCount)]
        patient = TreatedPatient(viruses, maxPopulation)
        for timeStep in range(0, medicationDelay):            
            patient.update()
        patient.addPrescription(drug)
        for timeStep in range(medicationDelay, totalTimeSteps - 1):
            patient.update()
        populationAtTrial[trial] = patient.update()
    return populationAtTrial
    
def drawHistogram():
    trials = 100
    drugName = 'guttagonol'
    resistances = {drugName: False}
    figure, plots = pylab.subplots(2, sharex=True, sharey=True)

    data = perTrialSimulationWithDrug(100, 1000, 0.1, 0.05, resistances, 0.005, trials)
    plots[0].hist(data, bins=50, histtype='stepfilled')
    plots[0].set_title('Non-Resistant')
    
    resistances[drugName] = True
    data = perTrialSimulationWithDrug(100, 1000, 0.1, 0.05, resistances, 0.005, trials)
    plots[1].hist(data, bins=50, histtype='stepfilled')
    plots[1].set_title('Resistant')
    
    pylab.show()
    
    
if '__main__' == __name__:
    resistances = {'guttagonol': False, 'grimpex': False}
    environment = SimulationEnvironment(100, 1000, 0.1, 0.05, resistances, 0.005, 100)
    Simulator().simulateCocktail(environment)
