# Problem Set 3: Simulating the Spread of Disease and Virus Population Dynamics 

import numpy
import random

from matplotlib import pylab

''' 
Begin helper code
'''

class NoChildException(Exception):
    """
    NoChildException is raised by the reproduce() method in the SimpleVirus
    and ResistantVirus classes to indicate that a virus particle does not
    reproduce. You can use NoChildException as is, you do not need to
    modify/add any code.
    """

'''
End helper code
'''

#
# PROBLEM 2
#
class SimpleVirus(object):

    """
    Representation of a simple virus (does not model drug effects/resistance).
    """
    def __init__(self, maxBirthProbability, clearanceProbability):
        """
        Initialize a SimpleVirus instance, saves all parameters as attributes
        of the instance.        
        maxBirthProbability: Maximum reproduction probability (a float between 0-1)        
        clearanceProbability: Maximum clearance probability (a float between 0-1).
        """
        self.maxBirthProbability = maxBirthProbability
        self.clearanceProbability = clearanceProbability

    def getMaxBirthProb(self):
        """
        Returns the max birth probability.
        """
        return self.maxBirthProbability

    def getClearProb(self):
        """
        Returns the clear probability.
        """
        return self.clearanceProbability

    def doesClear(self):
        """ Stochastically determines whether this virus particle is cleared from the
        patient's body at a time step. 
        returns: True with probability self.getClearProb and otherwise returns
        False.
        """
        return random.random() < self.clearanceProbability
    
    def reproduce(self, populationDensity):
        """
        Stochastically determines whether this virus particle reproduces at a
        time step. Called by the update() method in the Patient and
        TreatedPatient classes. The virus particle reproduces with probability
        self.maxBirthProb * (1 - popDensity).
        
        If this virus particle reproduces, then reproduce() creates and returns
        the instance of the offspring SimpleVirus (which has the same
        maxBirthProb and clearProb values as its parent).         

        popDensity: the population density (a float), defined as the current
        virus population divided by the maximum population.         
        
        returns: a new instance of the SimpleVirus class representing the
        offspring of this virus particle. The child should have the same
        maxBirthProb and clearProb values as this virus. Raises a
        NoChildException if this virus particle does not reproduce.               
        """
        effectiveProbability = self.maxBirthProbability * (1.0 - populationDensity)
        if (random.random() < effectiveProbability):
            return SimpleVirus(self.maxBirthProbability, self.clearanceProbability)
        else:
            raise NoChildException


class Patient(object):
    """
    Representation of a simplified patient. The patient does not take any drugs
    and his/her virus populations have no drug resistance.
    """    

    def __init__(self, viruses, maxVirusPopulation):
        """
        Initialization function, saves the viruses and maxPop parameters as
        attributes.

        viruses: the list representing the virus population (a list of
        SimpleVirus instances)

        maxPop: the maximum virus population for this patient (an integer)
        """
        self.viruses = viruses
        self.maxVirusPopulation = maxVirusPopulation

    def getViruses(self):
        """
        Returns the viruses in this Patient.
        """
        return self.viruses

    def getMaxPop(self):
        """
        Returns the max population.
        """
        return self.maxVirusPopulation

    def getTotalPop(self):
        """
        Gets the size of the current total virus population. 
        returns: The total virus population (an integer)
        """
        return len(self.viruses)

    def update(self):
        """
        Update the state of the virus population in this patient for a single
        time step. update() should execute the following steps in this order:
        
        - Determine whether each virus particle survives and updates the list
        of virus particles accordingly.   
        
        - The current population density is calculated. This population density
          value is used until the next call to update() 
        
        - Based on this value of population density, determine whether each 
          virus particle should reproduce and add offspring virus particles to 
          the list of viruses in this patient.                    

        returns: The total virus population at the end of the update (an
        integer)
        """
        populationDensity = float(self.getTotalPop()) / float(self.maxVirusPopulation)
        currentGeneration = [virus for virus in self.viruses]
        nextGeneration = []
        for virus in self.viruses:
            if (virus.doesClear()): currentGeneration.remove(virus)
        for virus in currentGeneration:
            populationDensity = float(len(currentGeneration) + len(nextGeneration)) / float(self.maxVirusPopulation)
            try:
                child = virus.reproduce(populationDensity)
                nextGeneration.append(child)
            except NoChildException:
                pass
        self.viruses = currentGeneration + nextGeneration
        return self.getTotalPop()
                

#
# PROBLEM 3
#
def simulationWithoutDrug(virusCount, maxPopulation, maxBirthProbability,
                          clearanceProbability, trials):
    """
    Run the simulation and plot the graph for problem 3 (no drugs are used,
    viruses do not have any drug resistance).    
    For each of numTrials trial, instantiates a patient, runs a simulation
    for 300 timesteps, and plots the average virus population size as a
    function of time.

    numViruses: number of SimpleVirus to create for patient (an integer)
    maxPop: maximum virus population for patient (an integer)
    maxBirthProb: Maximum reproduction probability (a float between 0-1)        
    clearProb: Maximum clearance probability (a float between 0-1)
    numTrials: number of simulation runs to execute (an integer)
    """
    viruses = [SimpleVirus(maxBirthProbability, clearanceProbability) for count in range(virusCount)]
    patient = Patient(viruses, maxPopulation)
    
    timeSteps = 300
    populationAtTime = [0 for count in range(timeSteps)]
    for trial in range(trials):
        for timeStep in range(timeSteps):
            populationAtTime[timeStep] += patient.update()
    averagePopulations = [float(population) / float(trials) for population in populationAtTime]

    pylab.plot(range(timeSteps), averagePopulations)
    pylab.title('Average Popluation of Viruses Over 300 Time Steps')
    pylab.xlabel('Time')
    pylab.ylabel('Average Population')
    pylab.legend(['No Drugs'], loc='lower right')
    pylab.show()

#
# PROBLEM 4
#
class ResistantVirus(SimpleVirus):
    """
    Representation of a virus which can have drug resistance.
    """   

    def __init__(self, maxBirthProbability, clearanceProbability, resistances,
                 mutationProbability):
        """
        Initialize a ResistantVirus instance, saves all parameters as attributes
        of the instance.

        maxBirthProb: Maximum reproduction probability (a float between 0-1)       

        clearProb: Maximum clearance probability (a float between 0-1).

        resistances: A dictionary of drug names (strings) mapping to the state
        of this virus particle's resistance (either True or False) to each drug.
        e.g. {'guttagonol':False, 'srinol':False}, means that this virus
        particle is resistant to neither guttagonol nor srinol.

        mutProb: Mutation probability for this virus particle (a float). This is
        the probability of the offspring acquiring or losing resistance to a drug.
        """
        SimpleVirus.__init__(self, maxBirthProbability, clearanceProbability)
        self.resistances = resistances
        self.mutationProbability = mutationProbability

    def getResistances(self):
        """
        Returns the resistances for this virus.
        """
        return self.resistances

    def getMutProb(self):
        """
        Returns the mutation probability for this virus.
        """
        return self.mutationProbability

    def isResistantTo(self, drug):
        """
        Get the state of this virus particle's resistance to a drug. This method
        is called by getResistPop() in TreatedPatient to determine how many virus
        particles have resistance to a drug.       

        drug: The drug (a string)

        returns: True if this virus instance is resistant to the drug, False
        otherwise.
        """
        return drug in self.resistances.keys() and self.resistances[drug]

    def reproduce(self, populationDensity, activeDrugs):
        """
        Stochastically determines whether this virus particle reproduces at a
        time step. Called by the update() method in the TreatedPatient class.

        A virus particle will only reproduce if it is resistant to ALL the drugs
        in the activeDrugs list. For example, if there are 2 drugs in the
        activeDrugs list, and the virus particle is resistant to 1 or no drugs,
        then it will NOT reproduce.

        Hence, if the virus is resistant to all drugs
        in activeDrugs, then the virus reproduces with probability:      

        self.maxBirthProb * (1 - popDensity).                       

        If this virus particle reproduces, then reproduce() creates and returns
        the instance of the offspring ResistantVirus (which has the same
        maxBirthProb and clearProb values as its parent). The offspring virus
        will have the same maxBirthProb, clearProb, and mutProb as the parent.

        For each drug resistance trait of the virus (i.e. each key of
        self.resistances), the offspring has probability 1-mutProb of
        inheriting that resistance trait from the parent, and probability
        mutProb of switching that resistance trait in the offspring.       

        For example, if a virus particle is resistant to guttagonol but not
        srinol, and self.mutProb is 0.1, then there is a 10% chance that
        that the offspring will lose resistance to guttagonol and a 90%
        chance that the offspring will be resistant to guttagonol.
        There is also a 10% chance that the offspring will gain resistance to
        srinol and a 90% chance that the offspring will not be resistant to
        srinol.

        popDensity: the population density (a float), defined as the current
        virus population divided by the maximum population       

        activeDrugs: a list of the drug names acting on this virus particle
        (a list of strings).

        returns: a new instance of the ResistantVirus class representing the
        offspring of this virus particle. The child should have the same
        maxBirthProb and clearProb values as this virus. Raises a
        NoChildException if this virus particle does not reproduce.
        """
        resistant = True
        for activeDrug in activeDrugs:
            resistant = resistant and self.isResistantTo(activeDrug)
            if not resistant: break

        birthProbability = self.maxBirthProbability * (1.0 - populationDensity)
        if resistant and random.random() < birthProbability:
            resistances = {}
            for drug, resistant in self.resistances.items():
                if (resistant):
                    resistances[drug] = random.random() > self.mutationProbability
                else:
                    resistances[drug] = random.random() < self.mutationProbability
            return ResistantVirus(self.maxBirthProbability, self.clearanceProbability, resistances, self.mutationProbability)
        else:        
            raise NoChildException
            

class TreatedPatient(Patient):
    """
    Representation of a patient. The patient is able to take drugs and his/her
    virus population can acquire resistance to the drugs he/she takes.
    """

    def __init__(self, viruses, maxPopulation):
        """
        Initialization function, saves the viruses and maxPop parameters as
        attributes. Also initializes the list of drugs being administered
        (which should initially include no drugs).              

        viruses: The list representing the virus population (a list of
        virus instances)

        maxPop: The  maximum virus population for this patient (an integer)
        """
        Patient.__init__(self, viruses, maxPopulation)
        self.prescriptions = []


    def addPrescription(self, newDrug):
        """
        Administer a drug to this patient. After a prescription is added, the
        drug acts on the virus population for all subsequent time steps. If the
        newDrug is already prescribed to this patient, the method has no effect.

        newDrug: The name of the drug to administer to the patient (a string).

        postcondition: The list of drugs being administered to a patient is updated
        """
        if (newDrug not in self.prescriptions): self.prescriptions.append(newDrug)


    def getPrescriptions(self):
        """
        Returns the drugs that are being administered to this patient.

        returns: The list of drug names (strings) being administered to this
        patient.
        """
        return self.prescriptions


    def getResistPop(self, drugs):
        """
        Get the population of virus particles resistant to the drugs listed in
        drugResist.       

        drugResist: Which drug resistances to include in the population (a list
        of strings - e.g. ['guttagonol'] or ['guttagonol', 'srinol'])

        returns: The population of viruses (an integer) with resistances to all
        drugs in the drugResist list.
        """
        count = 0
        for virus in self.viruses:
            resistant = True
            for drug in drugs: resistant = resistant and virus.isResistantTo(drug)
            if (resistant): count += 1
        return count


    def update(self):
        """
        Update the state of the virus population in this patient for a single
        time step. update() should execute these actions in order:

        - Determine whether each virus particle survives and update the list of
          virus particles accordingly

        - The current population density is calculated. This population density
          value is used until the next call to update().

        - Based on this value of population density, determine whether each 
          virus particle should reproduce and add offspring virus particles to 
          the list of viruses in this patient.
          The list of drugs being administered should be accounted for in the
          determination of whether each virus particle reproduces.

        returns: The total virus population at the end of the update (an
        integer)
        """
        currentGeneration = [virus for virus in self.viruses]
        nextGeneration = []
        for virus in self.viruses:
            if (virus.doesClear()): currentGeneration.remove(virus)
        populationDensity = float(len(currentGeneration)) / float(self.maxVirusPopulation)
        for virus in currentGeneration:            
            try:
                child = virus.reproduce(populationDensity, self.prescriptions)
                nextGeneration.append(child)
            except NoChildException:
                pass
        self.viruses = currentGeneration + nextGeneration
        return self.getTotalPop()


#
# PROBLEM 5
#
def simulationWithDrug(virusCount, maxPopulation, maxBirthProbability,
                       clearanceProbability, resistances, mutationProbability,
                       trials):
    """
    Runs simulations and plots graphs for problem 5.

    For each of numTrials trials, instantiates a patient, runs a simulation for
    150 timesteps, adds guttagonol, and runs the simulation for an additional
    150 timesteps.  At the end plots the average virus population size
    (for both the total virus population and the guttagonol-resistant virus
    population) as a function of time.

    numViruses: number of ResistantVirus to create for patient (an integer)
    maxPop: maximum virus population for patient (an integer)
    maxBirthProb: Maximum reproduction probability (a float between 0-1)        
    clearProb: maximum clearance probability (a float between 0-1)
    resistances: a dictionary of drugs that each ResistantVirus is resistant to
                 (e.g., {'guttagonol': False})
    mutProb: mutation probability for each ResistantVirus particle
             (a float between 0-1). 
    numTrials: number of simulation runs to execute (an integer)
    
    """
    viruses = [ResistantVirus(maxBirthProbability, clearanceProbability, resistances, mutationProbability) for count in range(virusCount)]
    patient = TreatedPatient(viruses, maxPopulation)

    totalTimeSteps = 300
    populationAtTime = [0 for count in range(totalTimeSteps)]
    resistantPopulationAtTime = [0 for count in range(totalTimeSteps)]

    drug = 'guttagonol'
    
    timeSteps = totalTimeSteps / 2
    for trial in range(trials):
        for timeStep in range(0, timeSteps):
            populationAtTime[timeStep] += patient.update()
            resistantPopulationAtTime[timeStep] += patient.getResistPop([drug])
        patient.addPrescription([drug])
        for timeStep in range(timeSteps, totalTimeSteps):
            populationAtTime[timeStep] += patient.update()
            resistantPopulationAtTime[timeStep] += patient.getResistPop([drug])

    averagePopulations = [float(population) / float(trials) for population in populationAtTime]
    averageResistantPopulations = [float(population) / float(trials) for population in resistantPopulationAtTime]            

    x = range(totalTimeSteps)
    pylab.plot(x, averagePopulations)
    pylab.plot(x, averageResistantPopulations)
    pylab.legend(['All', 'Resistant'], loc='lower right')

    pylab.title('Average Popluation of Viruses Over 300 Time Steps')
    pylab.xlabel('Time')
    pylab.ylabel('Average Population')
    pylab.show()
