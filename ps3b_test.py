#!/usr/bin/python

import random
import unittest

from ps3b import *
from mock import MagicMock, patch

class SimpleVirusTest(unittest.TestCase):

    def testDoesClear(self):
        #given:
        clearanceProbality = 0.6
        virus = SimpleVirus(0.5, clearanceProbality)

        #expect:
        with patch('random.random', side_effect=[0.601, 0.5999]) as mockRandom:
            self.assertFalse(virus.doesClear())
            self.assertTrue(virus.doesClear())

    def testReproduce(self):
        #given:
        birthProbability = 0.7
        populationDensity = 0.3
        parent = SimpleVirus(birthProbability, 0.5)

        #expect:
        with patch('random.random', side_effect=[0.37, 0.50]) as mockRandom:
            child = parent.reproduce(populationDensity)
            self.assertIsInstance(child, SimpleVirus)
            self.assertEquals(parent.getMaxBirthProb(), child.getMaxBirthProb())
            self.assertEquals(parent.getClearProb(), child.getClearProb())

            #and:
            with self.assertRaises(NoChildException):
                parent.reproduce(populationDensity)
    

class PatientTest(unittest.TestCase):

    def testUpdate(self):
        #given: 3 viruses: 1 clears, 1 lives but not reproduce,
        #1 lives and reproduces
        weakVirus = SimpleVirus(0.5, 0.5)
        weakVirus.doesClear = MagicMock(return_value=True)

        #and:
        fairlyStrongVirus = SimpleVirus(0.5, 0.5)
        fairlyStrongVirus.doesClear = MagicMock(return_value=False)
        fairlyStrongVirus.reproduce = MagicMock(side_effect=NoChildException())

        #and:
        strongVirus = SimpleVirus(0.5, 0.5)
        strongVirus.doesClear = MagicMock(return_value=False)
        childVirus = MagicMock()
        strongVirus.reproduce = MagicMock(return_value=childVirus)

        #and:
        patient = Patient([weakVirus, fairlyStrongVirus, strongVirus], 6)
        self.assertEquals(3, patient.getTotalPop())

        #when:
        currentPopulation = patient.update()

        #then:
        self.assertEquals(3, currentPopulation)

        #and:
        for survivingVirus in [fairlyStrongVirus, strongVirus, childVirus]:
            self.assertTrue(survivingVirus in patient.getViruses())
        weakVirus not in patient.getViruses()


class ResistantVirusTest(unittest.TestCase):

    def testIsResistantTo(self):
        #given:
        resistances = {'drug A': True, 'drug B': True, 'drug C': False}
        virus = ResistantVirus(0.1, 0.1, resistances, 0.1)

        #expect:
        self.assertTrue(virus.isResistantTo('drug A'))
        self.assertFalse(virus.isResistantTo('drug C'))
        self.assertFalse(virus.isResistantTo('drug Y'))
    
    def testReproduceWithNoResistance(self):
        #given:
        virus = ResistantVirus(0.2, 0.1, {'drug A': True, 'drug B': False}, 0.1)

        #expect:
        with self.assertRaises(NoChildException):
            virus.reproduce(0.4, ['drug B'])

    def testReproduceWithResistance(self):
        #given:
        resistances = {'drug A': True, 'drug B': False}
        virus = ResistantVirus(0.3, 0.15, resistances, 0.1)

        #expect:
        with patch('random.random', side_effect = [0.20, 0.5, 0.5, 0.22]):
            child = virus.reproduce(0.3, ['drug A'])
            self.assertIsInstance(child, ResistantVirus)

            #and:
            with self.assertRaises(NoChildException):
                child = virus.reproduce(0.3, ['drug A'])

    def testReproductionWithResistanceInheritance(self):
        #given:
        resistances = {'A': True, 'B': False, 'C': True, 'D': False}
        virus = ResistantVirus(0.3, 0.15, resistances, 0.1)
        
        #when:
        with patch('random.random', side_effect = [0.20, 0.21, 0.07, 0.87, 0.03]):
            child = virus.reproduce(0.3, ['A'])

        #then:
        self.assertTrue(child.isResistantTo('A'))
        self.assertTrue(child.isResistantTo('D'))

        #and:
        self.assertFalse(child.isResistantTo('B'))
        self.assertFalse(child.isResistantTo('C'))


class TreatedPatientTest(unittest.TestCase):

    def testAddPrescription(self):
        #given:
        patient = TreatedPatient([], 20)

        #when:
        patient.addPrescription('A')
        patient.addPrescription('B')
        patient.addPrescription('C')

        #and:
        patient.addPrescription('A')

        #then:
        self.assertEquals(3, len(patient.getPrescriptions()))

    def testGetResistPop(self):
        #given:
        virus1 = ResistantVirus(0.1, 0.1, {'A': True}, 0.1)
        virus2 = ResistantVirus(0.1, 0.1, {'A': True, 'B': True}, 0.1)
        virus3 = ResistantVirus(0.1, 0.1, {'A': True, 'B': True, 'C': True}, 0.1)

        #and:
        patient = TreatedPatient([virus1, virus2, virus3], 10)

        #expect:
        self.assertEquals(3, patient.getResistPop(['A']))
        self.assertEquals(2, patient.getResistPop(['A', 'B']))
        self.assertEquals(1, patient.getResistPop(['A', 'B', 'C']))

    def testUpdate(self):
        #given:
        weakVirus = ResistantVirus(0.1, 0.1, {}, 0.1)
        weakVirus.doesClear = MagicMock(return_value=True)

        #and:
        fairlyStrongVirus = ResistantVirus(0.1, 0.1, {}, 0.1)
        fairlyStrongVirus.doesClear = MagicMock(return_value=False)
        fairlyStrongVirus.reproduce = MagicMock(side_effect=NoChildException())

        #and:
        strongVirus = ResistantVirus(0.1, 0.1, {}, 0.1)
        strongVirus.doesClear = MagicMock(return_value=False)
        childVirus = MagicMock()
        strongVirus.reproduce = MagicMock(return_value=childVirus)

        #and:
        viruses = [weakVirus, fairlyStrongVirus, strongVirus]
        patient = TreatedPatient(viruses, 6)
        (patient.addPrescription(drug) for drug in ['A', 'B'])

        #when:
        population = patient.update()

        #then:
        self.assertEquals(3, population)

        #and:
        for virus in [fairlyStrongVirus, strongVirus, childVirus]:
            self.assertTrue(virus in patient.getViruses())


if __name__ == '__main__':
    unittest.main()
