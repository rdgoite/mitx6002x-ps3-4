#!/usr/bin/python

import numpy
import random
from matplotlib import pylab

#from ps3b_precompiled_27 import *
from ps3b import *

def simulationWithDrug(virusCount, maxPopulation, maxBirthProbability,
                       clearanceProbability, resistances, mutationProbability,
                       trials):
    """
    Runs simulations and plots graphs for problem 5.

    For each of numTrials trials, instantiates a patient, runs a simulation for
    150 timesteps, adds guttagonol, and runs the simulation for an additional
    150 timesteps.  At the end plots the average virus population size
    (for both the total virus population and the guttagonol-resistant virus
    population) as a function of time.

    numViruses: number of ResistantVirus to create for patient (an integer)
    maxPop: maximum virus population for patient (an integer)
    maxBirthProb: Maximum reproduction probability (a float between 0-1)        
    clearProb: maximum clearance probability (a float between 0-1)
    resistances: a dictionary of drugs that each ResistantVirus is resistant to
                 (e.g., {'guttagonol': False})
    mutProb: mutation probability for each ResistantVirus particle
             (a float between 0-1). 
    numTrials: number of simulation runs to execute (an integer)
    
    """
    totalTimeSteps = 300
    timeSteps = totalTimeSteps / 2

    populationAtTime = [0 for count in range(totalTimeSteps)]
    resistantPopulationAtTime = [0 for count in range(totalTimeSteps)]

    drug = 'guttagonol'

    for trial in range(trials):
        viruses = [ResistantVirus(maxBirthProbability, clearanceProbability, resistances, mutationProbability) for count in range(virusCount)]
        patient = TreatedPatient(viruses, maxPopulation)
        for timeStep in range(0, timeSteps):
            populationAtTime[timeStep] += patient.update()
            resistantPopulationAtTime[timeStep] += patient.getResistPop([drug])
        patient.addPrescription(drug)
        for timeStep in range(timeSteps, totalTimeSteps):
            populationAtTime[timeStep] += patient.update()
            resistantPopulationAtTime[timeStep] += patient.getResistPop([drug])

    averagePopulation = [float(population) / float(trials) for population in populationAtTime]
    averageResistantPopulation = [float(population) / float(trials) for population in resistantPopulationAtTime]

    timeAxis = range(totalTimeSteps)

    pylab.plot(timeAxis, averagePopulation)
    pylab.plot(timeAxis, averageResistantPopulation)
    pylab.legend(['All', 'Resistant'], loc='lower right')

    pylab.title('Average Popluation of Viruses Over 300 Time Steps')
    pylab.xlabel('Time')
    pylab.ylabel('Average Population')
    pylab.show()
